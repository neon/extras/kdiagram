# enolp <enolp@softastur.org>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2018-01-17 18:03+0100\n"
"Last-Translator: enolp <enolp@softastur.org>\n"
"Language-Team: Asturian <>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Qt-Contexts: true\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: kganttgraphicsview.cpp:196
msgctxt "KGantt::HeaderWidget|@title:menu"
msgid "Scale"
msgstr ""

#: kganttgraphicsview.cpp:200
msgctxt "KGantt::HeaderWidget|@item:inmenu Automatic scale"
msgid "Auto"
msgstr ""

#: kganttgraphicsview.cpp:203
msgctxt "KGantt::HeaderWidget|@item:inmenu"
msgid "Month"
msgstr ""

#: kganttgraphicsview.cpp:206
msgctxt "KGantt::HeaderWidget|@item:inmenu"
msgid "Week"
msgstr ""

#: kganttgraphicsview.cpp:209
msgctxt "KGantt::HeaderWidget|@item:inmenu"
msgid "Day"
msgstr "Día"

#: kganttgraphicsview.cpp:212
msgctxt "KGantt::HeaderWidget|@item:inmenu"
msgid "Hour"
msgstr ""

#: kganttgraphicsview.cpp:235
msgctxt "KGantt::HeaderWidget|@action:inmenu"
msgid "Zoom In"
msgstr ""

#: kganttgraphicsview.cpp:237
msgctxt "KGantt::HeaderWidget|@action:inmenu"
msgid "Zoom Out"
msgstr ""

#: kganttgraphicsview.cpp:241
msgctxt "KGantt::HeaderWidget|@action:inmenu"
msgid "Timeline..."
msgstr ""

#: kganttitemdelegate.cpp:154
#, qt-format
msgctxt "KGantt::ItemDelegate|start time -> end time: item name"
msgid "%1 -> %2: %3"
msgstr ""